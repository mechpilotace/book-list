# Book List

This is my booklist broken down by year to help track the books I am reading, and help drive the desire to read more. 

## 2020
### January
* 12 Rules For Life - Jordan B. Peterson

### February
* N/A

### March
* Unix & Linix Admin Handbook, 5th ed. (80% read) - Evi Nemeth [Crazy in depth, each chapter should be its own books.]

### April
* N/A

### May
* N/A

### June
* The Cathedral and the Bazaar - Eric S. Raymond
* The C Programming Language - K&R [This was a bit over my head. Would like to go back and read it again after I gain some more experince. Most people online use this as a Refrence.]

### July
* Started Reading: Antifragile - Nassim Taleb
* The Linux Command Line - William E. Shotts, Jr. (No Starch Press) [Finished 7/27]

### August
* Antifragile - Nassim Taleb [Another long and dense read Finished 8/26]
* Started Reading: Remembering the Kana - James W. Heisig [8/26]
### September
* N/A
### October
* N/A
### November
* The Eyes of the Dragon - Stephen King [11/3]

### December

-----

## 2021
### January
* Vacationland - John Hodgman [1-2-2021]
